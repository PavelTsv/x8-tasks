package SoftwareDevelopmentCycle;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by pavel on 25.08.17.
 */
public class SortByBits {
    public static void main(String[] args) {

    }

    public void sort(Integer[] numbers) {
        if (numbers.length == 0) {
            throw new IllegalArgumentException("Array is empty!");
        }
        Arrays.sort(numbers, new Comparator<Integer>() {

            @Override
            public int compare(Integer firstNumber, Integer secondNumber) {
                if (firstNumber == null || secondNumber == null) {
                    throw new IllegalArgumentException("Can not compare null numbers.");
                }
                return countOnes(firstNumber) - countOnes(secondNumber);
            }

        });
    }

    public static int countOnes(int number) {
        String intToBinary = Integer.toBinaryString(number);
        int onesCount = 0;
        for (int i = 0; i < intToBinary.length(); i++) {
            if (intToBinary.charAt(i) == '1') {
                onesCount++;
            }
        }
        return onesCount;
    }
}
