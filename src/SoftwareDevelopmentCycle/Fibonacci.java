package SoftwareDevelopmentCycle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by pavel on 14.08.17.
 */
public class Fibonacci {
    public static void main(String[] args) {
        Fibonacci fib = new Fibonacci();
        int[][] firstMatrix = {{13, 21, 34}, {55, 5, 89}, {144, 233, 377}};
        System.out.println(fib.findTheFibonacciElement(firstMatrix));

    }

    public int findTheFibonacciElement(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            throw new IllegalArgumentException("The matrix contains no elements.");
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (isFibonacciSequence(getNeighbours(matrix, i, j))) {
                    return matrix[i][j];
                }
            }
        }
        System.out.println("No fibonacci sequence found.");
        return -1;
    }

    public List getNeighbours(int[][] matrix, int row, int column) {
        List<Integer> neighbours = new ArrayList<Integer>();

        for (int i = Math.max(0, row - 1); i < Math.min(row + 2, matrix.length); i++) {
            for (int j = Math.max(0, column - 1); j < Math.min(column + 2, matrix[i].length); j++) {
                if (i == row && j == column) {
                    continue;
                }
                neighbours.add(matrix[i][j]);
            }
        }
        return neighbours;
    }


    public boolean isFibonacciSequence(List<Integer> matrixNeighbours) {
        if (matrixNeighbours.size() == 0) {
            throw new IllegalArgumentException("The matrix is empty. The element has no neighbours.");
        }
        int[] numbers = matrixNeighbours.stream().mapToInt(i -> i).toArray();
        Arrays.sort(numbers);

        int firstArrayNumber = numbers[0];
        int secondArrayNumber = numbers[1];

        if (firstArrayNumber + secondArrayNumber != getFibonacci(getFibonacciIndex(secondArrayNumber) + 1)) {
            return false;
        }

        if (!isFibNumber(firstArrayNumber) || !isFibNumber(secondArrayNumber)) {
            return false;
        }
        for (int i = 2; i < numbers.length; i++) {
            if (numbers[i - 2] + numbers[i - 1] != numbers[i] || !isFibNumber(numbers[i])) {
                return false;
            }
        }

        return true;
    }

    private final Integer getFibonacciIndex(int fibonacciNumber) {
        int result = -1;

        for (int i = 0; i <= fibonacciNumber; i++) {
            if (getFibonacci(i) == fibonacciNumber) {
                result = i;
            }
        }

        return result;
    }

    public boolean isFibNumber(long number) {
        if (number < 0) {
            throw new IllegalArgumentException("We don't support negative Fib numbers.");
        }
        return isPerfectSquare(5 * number * number + 4) || isPerfectSquare(5 * number * number - 4);
    }

    public final static boolean isPerfectSquare(long number) {
        if (number < 0)
            return false;

        long squaredNumber = (long) Math.sqrt(number);
        return squaredNumber * squaredNumber == number;
    }

    public int getFibonacci(int index) throws IllegalArgumentException {
        if (index < 0) {
            throw new IllegalArgumentException("We don't support negative Fib numbers.");
        } else if (index == 0) {
            return 0;
        } else if (index == 1) {
            return 1;
        } else {
            return getFibonacci(index - 1) + getFibonacci(index - 2);
        }
    }
}
