package dataStructuresAndAlgorithms;
/**
 * Created by pavel on 06.09.17.
 */
public class Flight {
    private final int arrivalTimeInMinutes;
    private int delayTimeInMinutes;

    public Flight(int arrivalTimeInMinutes) {
        this.arrivalTimeInMinutes = arrivalTimeInMinutes;
    }

    public int getArrivalTimeInMinutes() {
        return arrivalTimeInMinutes;
    }

    public void setDelayTimeInMinutes(int delay) {
        delayTimeInMinutes = delay;
    }

    public int getDelayTimeInMinutes() {
        return delayTimeInMinutes;
    }
}
