package dataStructuresAndAlgorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by pavel on 06.09.17.
 */
public class SimulateFlights {
    public static void main(String[] args) {
        final SimulateFlights simulateFlights = new SimulateFlights();
        simulateFlights.generateRandomFlights(100);
        simulateFlights.findDelayedFlights();
    }

    private List<Flight> flights = new ArrayList<>();
    private final Random random = new Random();

    private final int firstDayHourInMinutes = 0;
    private final int lastDayHourInMinutes = 1439;


    public void generateRandomFlights(int numberOfFlights) {

        for (int i = 0; i < numberOfFlights; i++) {
            int arrivalTime = random.nextInt(lastDayHourInMinutes) + firstDayHourInMinutes;
            flights.add(new Flight(arrivalTime));
        }
    }

    public void findDelayedFlights() {
        final int delayTimeNeededToPostInMinutes = 120;
        for (int currentTimeInMinutes = firstDayHourInMinutes; currentTimeInMinutes < lastDayHourInMinutes; currentTimeInMinutes++) {
            delayFlights();

            for (Flight flight : flights) {
                if (flight.getArrivalTimeInMinutes() + flight.getDelayTimeInMinutes() >= currentTimeInMinutes &&
                        flight.getDelayTimeInMinutes() >= delayTimeNeededToPostInMinutes) {
                    System.out.println("Post!");
                    flights.remove(flight);
                }
                if (flight.getArrivalTimeInMinutes() + flight.getDelayTimeInMinutes() <= currentTimeInMinutes) {
                    flights.remove(flight);
                }
            }
        }
    }

    private void delayFlights() {
        for (Flight flight : flights) {
            flight.setDelayTimeInMinutes(random.nextInt(lastDayHourInMinutes - flight.getArrivalTimeInMinutes()) + firstDayHourInMinutes);
        }
    }
}
