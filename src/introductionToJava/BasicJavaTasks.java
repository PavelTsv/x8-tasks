package introductionToJava;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Stack;

/**
 * Created by pavel on 23.08.17.
 */
public class BasicJavaTasks {
    public static void main(String[] args) {
    }

    public boolean isOdd(int n) {
        return n % 2 != 0;
    }

    public boolean isPrime(int N) {
        if (N < 2) {
            return false;
        }
        int squaredNumber = (int) Math.sqrt(N);

        for (int i = 2; i <= squaredNumber; i++) {
            if (N % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int min(int... array) {
        if (array.length == 0) {
            throw new IllegalArgumentException("Array is empty.");
        }
        int min = array[0];

        for (int i = 1; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
            }
        }
        return min;
    }

    public int getOddOccurrence(int... array) {
        if (array.length == 0) {
            throw new IllegalArgumentException("Array is empty.");
        }
        int occurrence = 1;
        Arrays.sort(array);

        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) {
                occurrence++;
            } else if (isOdd(occurrence)) {
                return array[i];
            } else {
                occurrence = 1;
            }
        }

        return -999;//No number exists odd times.
    }

    public int getAverage(int[] array) {
        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return array.length == 0 ? sum : sum / array.length;
    }

    public long pow(int a, int b) {
        if (b < 0) {
            throw new IllegalArgumentException("Negative powers don't work with type long.");
        } else if (b == 0) {
            return 1;
        } else if (b == 1) {
            return a;
        } else if (b % 2 == 0) {
            return pow(a * a, b / 2);
        } else {
            return a * pow(a * a, (b - 1) / 2);
        }
    }

    public long getSmallestMultiple(int N) {
        boolean canBeDivided;
        long smallestMultiple = -1;

        for (int i = N; i < Integer.MAX_VALUE; i++) {
            if (N < 1) {
                throw new IllegalArgumentException("N must be bigger than 1.");
            }
            canBeDivided = true;
            for (int j = 2; j <= N; j++) {
                if (i % j != 0) {
                    canBeDivided = false;
                    break;
                }
            }
            if (canBeDivided) {
                smallestMultiple = i;
                break;
            }
        }
        return smallestMultiple;
    }

    public long doubleFac(int n) {
        return factorial((int) factorial(n));
    }

    private long factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("n can not be smaller than zero.");
        }
        long factorial = 1;

        for (int i = n; i >= 2; i--) {
            factorial *= i;
        }

        return factorial;
    }

    public long maximalScalarSum(int[] a, int[] b) {
        Arrays.sort(a);
        Arrays.sort(b);

        return calculateScalarSum(a, b);

    }

    private static int calculateScalarSum(int[] vectorA, int[] vectorB) {
        if (vectorA.length != vectorB.length) {
            throw new IllegalArgumentException("The size of both of the arrays must be equal.");
        }

        int sum = 0;

        for (int i = 0; i < vectorA.length; i++) {
            sum += vectorA[i] * vectorB[i];
        }
        return sum;
    }

    public int maxSpan(int[] numbers) {
        int maxSpan = 0;
        int count;

        for (int i = 0; i < numbers.length; i++) {
            count = 1;
            for (int j = i + 1; j < numbers.length; j++) {
                count++;
                if (numbers[i] == numbers[j] && count > maxSpan) {
                    maxSpan = count;
                }
            }
        }
        return maxSpan;
    }

    public boolean equalSumSides(int[] numbers) {
        if (numbers.length == 0) {
            throw new IllegalArgumentException("Array is empty!");
        }
        boolean result = false;

        for (int i = 0; i < numbers.length; i++) {
            if (calculateLeftSideSum(numbers, i) == calculateRightSideSum(numbers, i)) {
                result = true;
                break;
            }
        }
        return result;
    }

    private int calculateLeftSideSum(int[] numbers, int index) {
        int sum = 0;
        for (int i = 0; i < index - 1; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    private int calculateRightSideSum(int[] numbers, int index) {
        int sum = 0;
        for (int i = index + 1; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    public String reverse(String argument) {
        String reversedString = "";

        for (int i = 0; i < argument.length(); i++) {
            reversedString += argument.charAt(argument.length() - 1 - i);
        }
        return reversedString;
    }

    public String reverseEveryWord(String arg) {
        String everyWordReversed = arg;

        for (String word : arg.split(" ")) {
            everyWordReversed = everyWordReversed.replace(word, reverse(word));
        }

        return everyWordReversed;
    }

    public boolean isPalindrome(String argument) {
        String palindrome = "";
        for (String word : argument.split("\\W+")) {
            palindrome += word;
        }
        return palindrome.toLowerCase().equals(reverse(palindrome.toLowerCase()));
    }

    public boolean isPalindrome(int argument) {
        if (argument < 0) {
            argument = argument * -1;
        }
        return argument == Integer.parseInt(reverse(String.valueOf(argument)));
    }

    public boolean isPalindrome(long argument) {
        if (argument < 0) {
            argument = argument * -1;
        }
        return argument == Long.parseLong(reverse(String.valueOf(argument)));
    }

    public long getLargestPalindrome(long N) {
        return isPalindrome(N) ? N : getLargestPalindrome(--N);
    }

    public String copyChars(String input, int k) {
        StringBuilder output = new StringBuilder();

        if (k == 0) {
            output.append("");
        } else if (k < 0) {
            throw new IllegalArgumentException("K must be bigger or equal to 0");
        } else {
            for (int i = 0; i < k; i++) {
                output.append(input);
            }
        }

        return output.toString();

    }

    public int mentions(String word, String text) {
        int count = 0;
        int index = 0;

        if (!text.contains(word) || word == "") {
            return count;
        }

        while ((index = text.toLowerCase().indexOf(word.toLowerCase(), index)) != -1) {
            count++;
            index += word.length();
        }
        return count;
    }

    public String decodeUrl(String input) {
        return input.replaceAll("%20", " ").replaceAll("%3A", ":").replaceAll("%3D", "?").replaceAll("%2F", "/");
    }

    public int sumOfNumbers(String input) {
        int sum = 0;

        if (input != "") {
            for (String numbers : input.replaceAll("[^-0-9]+", " ").split("\\s+")) {
                if (isNumeric(numbers)) {
                    sum += Integer.parseInt(numbers);
                }
            }
        }

        return sum;

    }

    private static boolean isNumeric(String s) {
        if (s == "") {
            throw new IllegalArgumentException("String is empty");
        }
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }

    public boolean anagram(String A, String B) {
        char[] firstWord = A.replaceAll("[\\s]", "").toLowerCase().toCharArray();
        char[] secondWord = B.replaceAll("[\\s]", "").toLowerCase().toCharArray();
        Arrays.sort(firstWord);
        Arrays.sort(secondWord);
        return Arrays.equals(firstWord, secondWord);

    }

    public boolean hasAnagramOf(String A, String B) {
        if (A.length() == 0) {
            throw new IllegalArgumentException("A is empty.");
        } else if (B.length() == 0) {
            throw new IllegalArgumentException("B is empty.");
        }
        boolean hasAnagram = false;

        int lengthOfB = B.length();
        int lengthOfA = A.length();

        for (int i = 0; i < lengthOfA; i++) {
            if (i + lengthOfB < lengthOfA) {
                if (anagram(A.substring(i, i + lengthOfB), B)) {
                    hasAnagram = true;
                }
            } else {
                break;
            }
        }

        return hasAnagram;
    }

    public int[] histogram(short[][] image) {
        int[] result = new int[256];

        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[i].length; j++) {
                if (image[i][j] < 0) {
                    throw new IllegalArgumentException("Element is smaller than 0.");
                }
                result[image[i][j]]++;
            }
        }
        return result;
    }

    public int kthMin(int k, int[] array) {
        if (k > array.length - 1 || k < 0) {
            throw new IllegalArgumentException("K needs to be smaller than the Array size or bigger than Zero.");
        }
        Arrays.sort(array);

        return array[k - 1];
    }

    public long kthFac(int k, int n) {
        if (k < 0) {
            throw new IllegalArgumentException("K must be bigger or equal to 0.");
        }
        long result = n;

        for (int i = 0; i < k; i++) {
            result = factorial((int) result);
        }
        return result;
    }

    public int[][] rescale(int[][] original, int newWidth, int newHeight) {
        if (original == null || original.length == 0 || original[0].length == 0) {
            throw new IllegalArgumentException("Matrix is empty.");
        }

        int originalHeight = original.length;
        int originalWidth = original[0].length;

        int[][] rescaledMatrix = new int[newHeight][newWidth];
        double heightRatio = originalHeight / (double) newHeight;
        double widthRation = originalWidth / (double) newWidth;
        int pixelI;
        int pixelJ;

        for (int i = 0; i < newHeight; i++) {
            for (int j = 0; j < newWidth; j++) {
                pixelI = (int) Math.floor(i * heightRatio);
                pixelJ = (int) Math.floor(j * widthRation);
                rescaledMatrix[i][j] = original[pixelI][pixelJ];
            }
        }
        return rescaledMatrix;
    }

    public void convertToGreyscale(String path) {
        BufferedImage image = getImage(path);

        for (int x = 0; x < image.getWidth(); ++x) {
            for (int y = 0; y < image.getHeight(); ++y) {
                int rgb = image.getRGB(x, y);
                int r = (rgb >> 16) & 0xFF;
                int g = (rgb >> 8) & 0xFF;
                int b = (rgb & 0xFF);

                int greyLevel = (r + g + b) / 3;
                int grey = (greyLevel << 16) + (greyLevel << 8) + greyLevel;
                image.setRGB(x, y, grey);
            }
        }
        createImage(image);
    }

    private BufferedImage getImage(String path) {
        BufferedImage image = null;

        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
            System.out.println("Incorrect class path.");
        }
        return image;
    }

    private void createImage(BufferedImage image) {
        try {
            File outputFile = new File("lib/Greyscale.png");
            ImageIO.write(image, "png", outputFile);
        } catch (IOException e) {
            System.out.println("Could not create file.");
        }
    }
}
