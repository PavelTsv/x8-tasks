package introductionToJava;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import static org.junit.Assert.*;

/**
 * Created by pavel on 23.08.17.
 */
public class BasicJavaTasksTest {

    private final BasicJavaTasks tasks = new BasicJavaTasks();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void givenPositiveEvenNumberWhenCallingIsOddNumberThenReturnFalse() {
        final int positiveEvenNumber = 6;
        assertFalse(tasks.isOdd(positiveEvenNumber));
    }

    @Test
    public void givenNegativeEvenNumberWhenCallingIsOddNumberThenReturnFalse() {
        final int negativeEvenNumber = -8;
        assertFalse(tasks.isOdd(negativeEvenNumber));
    }

    @Test
    public void givenPositiveOddNumberWhenCallingIsOddNumberThenReturnTrue() {
        final int positiveOddNumber = 5;
        assertTrue(tasks.isOdd(positiveOddNumber));
    }

    @Test
    public void givenNegativeOddNumberWhenCallingIsOddNumberThenReturnTrue() {
        final int negativeOddNumber = -13;
        assertTrue(tasks.isOdd(negativeOddNumber));
    }

    @Test
    public void givenZeroWhenCallingIsOddNumberThenReturnFalse() {
        assertFalse(tasks.isOdd(0));
    }

    @Test
    public void givenNegativeNumberWhenCallingIsPrimeThenReturnFalse() {
        int negativeNumber = -7;
        assertFalse(tasks.isPrime(negativeNumber));
    }

    @Test
    public void givenPrimeNumberWhenCallingIsPrimeThenReturnTrue() {
        int primeNumber = 13;
        assertTrue(tasks.isPrime(primeNumber));
    }

    @Test
    public void givenNonPrimeNumberWhenCallingIsPrimeThenReturnFalse() {
        int nonPrimeNumber = 6;
        assertFalse(tasks.isPrime(nonPrimeNumber));
    }

    @Test
    public void givenEmptyArrayWhenCallingMinThenThrowException() {
        exception.expect(IllegalArgumentException.class);
        tasks.min();
    }

    @Test
    public void givenEqualNumbersInArrayWhenCallingMinThenReturnSameNumber() {
        assertEquals(2, tasks.min(2, 2, 2, 2));
    }

    @Test
    public void givenNegativeArrayWhenCallingMinThenReturnMin() {
        assertEquals(-23, tasks.min(-7, -11, -23, -1, -2));
    }

    @Test
    public void givenPositiveArrayWhenCallingMinThenReturnMin() {
        assertEquals(3, tasks.min(28, 5, 3, 23, 33, 43, 29));
    }

    @Test
    public void givenMixedArrayWhenCallingMinThenReturnMin() {
        assertEquals(-11, tasks.min(-11, 3, 4, -7, 1, 11, 4));
    }

    @Test
    public void givenEmptyArrayWhenCallingGetOddOccurrenceThenThrowException() {
        exception.expect(IllegalArgumentException.class);
        tasks.getOddOccurrence();
    }

    @Test
    public void givenNoOddOccurrenceWhenCallingGetOddOccurrenceThenReturnDefaultValue() {
        int noNumberExistsOddTimes = -999;
        assertEquals(noNumberExistsOddTimes, tasks.getOddOccurrence(4, 2, 2, 4));
    }

    @Test
    public void giveOddOccurrenceWhenCallingGetOddOccurrenceThenReturnOddOccurredNumber() {
        assertEquals(3, tasks.getOddOccurrence(2, 5, 3, 7, 3, 4, 2, 3));
    }

    @Test
    public void givenEmptyArrayWhenGetAverageInvokedThenReturnZero() {
        assertEquals(0, tasks.getAverage(new int[]{}));
    }

    private int expectedAverageNumber;

    @Test
    public void givenArrayWithPositiveNumbersWhenGetAverageInvokedThenReturnAverage() {
        expectedAverageNumber = 6;
        int[] positivNumbers = new int[]{3, 4, 11, 5, 7};
        assertEquals(expectedAverageNumber, tasks.getAverage(positivNumbers));
    }

    @Test
    public void givenArrayWithNegativeNumbersWhenGetAverageInvokedThenReturnAverage() {
        expectedAverageNumber = -8;
        int[] negativeNumbers = new int[]{-11, -23, -3, -4, -1, -6};
        assertEquals(expectedAverageNumber, tasks.getAverage(negativeNumbers));
    }

    @Test
    public void givenMixedArrayWhenGetAverageInvokedThenReturnAverage() {
        expectedAverageNumber = -3;
        int[] mixedNumbers = new int[]{-40, 27, 5, 13, -8, -15};
        assertEquals(expectedAverageNumber, tasks.getAverage(mixedNumbers));
    }

    @Test
    public void givenZeroAsABaseWhenPowInvokedThenReturnZero() {
        assertEquals(0, tasks.pow(0, 3));
    }

    @Test
    public void givenZeroAsPowerWhenPowInvokedThenReturnOne() {
        assertEquals(1, tasks.pow(3, 0));
    }

    @Test
    public void givenPositiveBaseWhenPowerInvokedThenReturnPositiveNumber() {
        assertEquals(64, tasks.pow(4, 3));
    }

    @Test
    public void givenPositiveBaseAndNegativePowerWhenPowInvokedThenReturnPositiveNumber() {
        exception.expect(IllegalArgumentException.class);
        tasks.pow(3, -2);
    }

    @Test
    public void givenNegativeBaseAndPowerWhenPowInvokedThenReturnNegativeNumber() {
        exception.expect(IllegalArgumentException.class);
        tasks.pow(-3, -3);
    }

    @Test
    public void givenNegativeBaseAndPositiveEvenPowerWhenPowInvokedThenReturnPositiveNumber() {
        assertEquals(49, tasks.pow(-7, 2));
    }

    @Test
    public void givenNegativeBaseAndPositiveOddPowerWhenPowInvokedThenReturnNegativeNumber() {
        assertEquals(-27, tasks.pow(-3, 3));
    }

    @Test
    public void giveNumberSmallerThanOneWhenGetSmallestMultipleInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.getSmallestMultiple(0);
    }

    @Test
    public void givenANumberBiggerThanOneWhenGetSmallestMultipleInvokedThenReturnSmallestMultiple() {
        assertEquals(840, tasks.getSmallestMultiple(8));
    }

    @Test
    public void givenANumberAboveOneWhenDoubleFacInvokedThenReturnDoubleFib() {
        assertEquals(720, tasks.doubleFac(3));
    }

    @Test
    public void givenNegativeNumberWhenDoubleFacInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.doubleFac(-3);
    }

    @Test
    public void givenEmptyArraysWhenMaximalScalarSumInvokedThenReturnZero() {
        assertEquals(0, tasks.maximalScalarSum(new int[]{}, new int[]{}));
    }

    @Test
    public void givenTwoNegativeArraysWhenMaximalScalarSumInvokedThenReturnPositiveScalarSum() {
        assertEquals(35, tasks.maximalScalarSum(new int[]{-1, -3, -7}, new int[]{-4, -1, -2}));
    }

    @Test
    public void givenTwoPositiveArraysWhenMaximalScalarSumInvokedThenReturnPositiveScalarSum() {
        assertEquals(53, tasks.maximalScalarSum(new int[]{5, 3, 4}, new int[]{3, 7, 2}));
    }

    @Test
    public void givenMixedArrayWhenMaximalScalarSumInvokedThenReturnScalarSum() {
        assertEquals(32, tasks.maximalScalarSum(new int[]{3, -7, -5}, new int[]{4, -5, 3}));
    }

    @Test
    public void givenEmptyArrayWhenMaxSpanInvokedThenReturnZero() {
        assertEquals(0, tasks.maxSpan(new int[]{}));
    }

    @Test
    public void givenAnArrayWhenMaxSpanInvokedThenReturnMaxSpan() {
        assertEquals(4, tasks.maxSpan(new int[]{2, 5, 4, 1, 3, 4}));
    }

    @Test
    public void givenAnArrayWithDuplicateNumbersWhenMaxSpanInvokedThenReturnMaxSpan() {
        assertEquals(6, tasks.maxSpan(new int[]{3, 6, 6, 8, 4, 3, 6}));
    }

    @Test
    public void givenEmptyArrayWhenEqualSumSidesInvokedThenThrowException() {
        exception.expect(IllegalArgumentException.class);
        tasks.equalSumSides(new int[]{});
    }

    @Test
    public void givenSingleElementWhenEqualSumSidesInvokedThenReturnTrue() {
        assertTrue(tasks.equalSumSides(new int[]{1}));
    }

    @Test
    public void givenAZeroAndAnotherNumberWhenEqualSumSidesInvokedThenReturnTrue() {
        assertTrue(tasks.equalSumSides(new int[]{1, 0}));
    }

    @Test
    public void givenAnArrayWhenEqualSumSidesInvokedThenReturnTrue() {
        assertTrue(tasks.equalSumSides(new int[]{1, -7, 9, 5, -12, 9, 4, 2, -6}));
    }

    @Test
    public void givenMixedCapsSignsStringWhenReverseInvokedThenReturnReversedString() {
        assertEquals("Johny£27", tasks.reverse("72£ynhoJ"));
    }

    @Test
    public void givenEmptyStringWhenReverseInvokedThenReturnEmptyString() {
        assertEquals("", tasks.reverse(""));
    }

    @Test
    public void givenAllCapsStringWhenReverseInvokedThenReturnAllCapsString() {
        assertEquals("OLLEH", tasks.reverse("HELLO"));
    }

    @Test
    public void givenAllSmallCapsWhenReverseInvokedThenReturnAllSmallCaps() {
        assertEquals("eurt", tasks.reverse("true"));
    }

    @Test
    public void givenEmptySpaceBeforeWordWhenReverseInvokedThenReturnReversedWord() {
        assertEquals(" wolleY", tasks.reverse("Yellow "));
    }

    @Test
    public void givenEmptySpaceAtTheEndOfTheWordWhenReverseInvokedThenReturnReversedWord() {
        assertEquals("trid ", tasks.reverse(" dirt"));
    }

    @Test
    public void givenAStringSentenceStringWhenReverseEveryWordInvokedThenReturnEveryWordReversed() {
        assertEquals("evoL si ni eht !ria", tasks.reverseEveryWord("Love is in the air!"));
    }

    @Test
    public void givenAllCapsSentenceStringWhenReverseEveryWordInvokedThenReturnAllCapsWords() {
        assertEquals("SI YDOBYNA EREHT", tasks.reverseEveryWord("IS ANYBODY THERE"));
    }

    @Test
    public void givenAllSmallCapsSentenceStringWhenReverseEveryWordInvokedThenReturnAllSmallCaps() {
        assertEquals("tahw si evol", tasks.reverseEveryWord("what is love"));
    }

    @Test
    public void givenAllSymbolAndNumbersStringWhenReverseEveryWordInvokedThenReturnAllReversedSymbols() {
        assertEquals("£$^ $$$ ^&^", tasks.reverseEveryWord("^$£ $$$ ^&^"));
    }

    @Test
    public void givenEmptyStringWhenReverseEveryWordInvokedThenReturnEmptyString() {
        assertEquals("", tasks.reverseEveryWord(""));
    }

    @Test
    public void givenAPalindromeStringWhenIsPalindromeInvokedThenReturnTrue() {
        assertTrue(tasks.isPalindrome("A man, a plan, a canal, Panama!"));
    }

    @Test
    public void givenEmptyStringWhenIsPalindromeInvokedThenReturnTrue() {
        assertTrue(tasks.isPalindrome(""));
    }

    @Test
    public void givenANumberPalindromeWhenIsPalindromeInvokedThenReturnTrue() {
        assertTrue(tasks.isPalindrome(12321));
    }

    @Test
    public void givenASingleNumberWhenIsPalindromeInvokedThenReturnTrue() {
        assertTrue(tasks.isPalindrome(1));
    }

    @Test
    public void givenANegativeNumberPalindromeWhenIsPalindromeInvokedThenReturnTrue() {
        assertTrue(tasks.isPalindrome(-34243));
    }

    @Test
    public void givenAPositiveNumberWhenGetLargestPalindromeInvokedThenReturnLargestPalindrome() {
        assertEquals(2343432, tasks.getLargestPalindrome(2344112));
    }

    @Test
    public void givenANegativeNumberWhenGetLargestPalindromeInvokedTHenReturnLargestPalindrome() {
        assertEquals(-1455541, tasks.getLargestPalindrome(-1455321));
    }

    @Test
    public void givenNegativeNumberWhenCopyCharsInvokedThenIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.copyChars("Hello", -1);
    }

    @Test
    public void givenZeroWhenCopyCharsInvokedThenReturnEmptyString() {
        assertEquals("", tasks.copyChars("33A", 0));
    }

    @Test
    public void givenANumberBiggerThanZeroWhenCopyCharsInvokedThenReturnStringMultipliedKTimes() {
        assertEquals("Monkey?Monkey?Monkey?", tasks.copyChars("Monkey?", 3));
    }

    @Test
    public void givenEmptyStringWhenCopyCharsInvokedThenReturnEmptyString() {
        assertEquals("", tasks.copyChars("", 4));
    }

    @Test
    public void givenNoMentionsOfWordWhenMentionsInvokedThenReturnZero() {
        assertEquals(0, tasks.mentions("Dog", "CatsAndMonkeys"));
    }

    @Test
    public void givenEmptyStringWhenMentionsInvokedThenReturnZero() {
        assertEquals(0, tasks.mentions("Cat", ""));
    }

    @Test
    public void givenNoWordWhenMentionsInvokedThenReturnZero() {
        assertEquals(0, tasks.mentions("", "CatsAndDogs"));
    }

    @Test
    public void givenAWordIgnoringCaseWhenMentionsInvokedThenReturnCount() {
        assertEquals(4, tasks.mentions("what", "WhattfwahtfwhataWhathwatwhat"));
    }

    @Test
    public void givenAStringUrlWithDashAndDoubleDotReplaceCharactersWhenDecodeUrlInvokedThenReturnDecodedUrl() {
        String expected = "https://en.wikipedia.org/wiki/Java_(programming_language)";
        String actual = tasks.decodeUrl("https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FJava_(programming_language)");
        assertEquals(expected, actual);
    }

    @Test
    public void givenAStringUrlWitQuestionMarkAndQuotesWhenDecodeUrlInvokedTHenReturnDecodedUrl() {
        String expected = "Dogs ? : Cats";
        String actual = tasks.decodeUrl("Dogs%20%3D%20%3A%20Cats");
        assertEquals(expected, actual);
    }

    @Test
    public void givenStringWithOutNumbersWhenSumOfNumbersInvokedThenReturnZero() {
        assertEquals(0, tasks.sumOfNumbers("Hello"));
    }

    @Test
    public void givenEmptyStringWhenSumOfNumbersInvokedTHenReturnZero() {
        assertEquals(0, tasks.sumOfNumbers(""));
    }

    @Test
    public void givenNumbersInAStringWhenSumOfNumbersInvokedThenReturnSum() {
        assertEquals(45, tasks.sumOfNumbers("Hello23Java, nice 2 meet you20."));
    }

    @Test
    public void givenNegativeNumbersInAStringWhenSumOfNumbersInvokedThenReturnSum() {
        assertEquals(20, tasks.sumOfNumbers("Gr33tings, i am L-13."));
    }

    @Test
    public void givenTwoStringsIgnoringCaseWhenAnagramInvokedThenReturnTrue() {
        assertTrue(tasks.anagram("William Shakespeare", "I am a weakish speller"));
    }

    @Test
    public void givenTwoEmptyStringsWhenAnagramInvokedThenReturnTrue() {
        assertTrue(tasks.anagram("", ""));
    }

    @Test

    public void givenTwoEmptyStringWhenHasAnagramOfInvokedThenReturnTrue() {
        exception.expect(IllegalArgumentException.class);
        tasks.hasAnagramOf("", "");
    }

    @Test
    public void givenEmptyStringAndSubstringWhenHasAnagramOfInvokedThenReturnFalse() {
        exception.expect(IllegalArgumentException.class);
        tasks.hasAnagramOf("", "Dog");
    }

    @Test
    public void givenAStringAndEmptySubstringWhenHasAnagramOfInvokedThenReturnFalse() {
        exception.expect(IllegalArgumentException.class);
        tasks.hasAnagramOf("Dogs and cats.", "");
    }

    @Test
    public void givenAStringAndSubstringWhenHasAnagramInvokedThenReturnTrue() {
        assertTrue(tasks.hasAnagramOf("I will defy nature", "tura"));
    }

    @Test
    public void givenEmptyMatrixWhenHistogramInvokedThenReturnAllZeroesMatrix() {
        int[] expected = new int[256];
        assertArrayEquals(expected, tasks.histogram(new short[][]{}));
    }

    @Test
    public void givenAMatrixWhenHistogramInvokedThenReturnExpectedNumbers() {
        int[] expected = new int[256];
        expected[3] = 2;
        expected[5] = 1;
        assertArrayEquals(expected, tasks.histogram(new short[][]{{3, 5}, {3}}));
    }

    @Test
    public void givenNegativeNumberWhenHistogramInvokedThenThrowException() {
        exception.expect(IllegalArgumentException.class);
        tasks.histogram(new short[][]{{-3}});
    }

    @Test
    public void givenNumberBiggerThanArraySizeWhenKthMinInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.kthMin(3, new int[]{3, 2,});
    }

    @Test
    public void givenNumberSmallerThanZeroWhenKthMinInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.kthMin(-1, new int[]{1});
    }

    @Test
    public void givenNumberWhenKthMinInvokedThenKthSmallestNumbers() {
        assertEquals(7, tasks.kthMin(3, new int[]{7, 10, 4, 3, 20, 15}));
    }

    @Test
    public void givenPositiveNumberWhenKthFactorialInvokedThenReturnKthFactorialOfNumber() {
        assertEquals(720, tasks.kthFac(2, 3));
    }

    @Test
    public void givenNegativeKWhenKthFactorialInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.kthFac(-3, 2);
    }

    @Test
    public void givenNumberBelowZeroWhenKthFactorialInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.kthFac(2, -3);
    }

    private final int[][] image = {
            {3, 7},
            {4, 8}
    };
    private final int[][] biggerImage = {
            {3, 5, 7, 9},
            {4, 6, 8, 10},
            {5, 7, 9, 11},
            {6, 8, 10, 12}
    };
    int newHeight;
    int newWidth;

    @Test
    public void givenEvenEqualWidthHeightAndUpscaleWhenRescaleInvokedThenReturnUpScaledImage() {
        newHeight = 4;
        newWidth = 4;
        int[][] expectedImage = {
                {3, 3, 7, 7},
                {3, 3, 7, 7},
                {4, 4, 8, 8},
                {4, 4, 8, 8}
        };
        assertArrayEquals(expectedImage, tasks.rescale(image, newWidth, newHeight));
    }

    @Test
    public void givenOddEqualWidthHeightAndUpscaleWhenRescaleInvokedThenReturnUpScaledImage() {
        newHeight = 3;
        newWidth = 3;
        int[][] expectedImage = {
                {3, 3, 7},
                {3, 3, 7},
                {4, 4, 8}
        };
        assertArrayEquals(expectedImage, tasks.rescale(image, newWidth, newHeight));
    }

    @Test
    public void givenEvenUnequalWidthHeightAndUpscaleWhenRescaleInvokedThenReturnUpScaledImage() {
        newHeight = 4;
        newWidth = 6;
        int[][] expectedImage = {
                {3, 3, 3, 7, 7, 7},
                {3, 3, 3, 7, 7, 7},
                {4, 4, 4, 8, 8, 8},
                {4, 4, 4, 8, 8, 8}
        };
        assertArrayEquals(expectedImage, tasks.rescale(image, newWidth, newHeight));
    }

    @Test
    public void givenOddUnequalWidthHeightAndUpscaleWhenRescaleInvokedThenReturnUpScaledImage() {
        newHeight = 3;
        newWidth = 5;
        int[][] expectedImage = {
                {3, 3, 3, 7, 7},
                {3, 3, 3, 7, 7},
                {4, 4, 4, 8, 8}
        };
        assertArrayEquals(expectedImage, tasks.rescale(image, newWidth, newHeight));
    }

    @Test
    public void givenWithHeightEqualToOneAndDownScaleWhenRescaleInvokedThenReturnSinglePixelImage() {
        newHeight = 1;
        newWidth = 1;
        int[][] expectedImage = {
                {3}
        };
        assertArrayEquals(expectedImage, tasks.rescale(image, newWidth, newHeight));
    }

    @Test
    public void givenOddEqualWithHeightAndDownScaleWhenRescaleInvokedThenReturnDownScaledImage() {
        newHeight = 3;
        newWidth = 3;
        int[][] expectedImage = {
                {3, 5, 7},
                {4, 6, 8},
                {5, 7, 9}
        };
        assertArrayEquals(expectedImage, tasks.rescale(biggerImage, newWidth, newHeight));
    }

    @Test
    public void givenOddUnEqualWidthHeightAndDownscaleWhenRescaleInvokedThenReturnDownScaledImage() {
        newHeight = 1;
        newWidth = 3;
        int[][] expectedImage = {
                {3, 5, 7}
        };
        assertArrayEquals(expectedImage, tasks.rescale(biggerImage, newWidth, newHeight));
    }

    @Test
    public void givenEmptyImageWhenRescaleInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        tasks.rescale(new int[][]{}, 2, 2);
    }

    @Test
    public void givenWidthHeightEqualToZeroThenReturnEmptyImage() {
        newHeight = 0;
        newWidth = 0;
        int[][] expectedImage = {};
        assertArrayEquals(expectedImage, tasks.rescale(image, newWidth, newHeight));
    }
}