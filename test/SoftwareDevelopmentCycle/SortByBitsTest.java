package SoftwareDevelopmentCycle;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Created by pavel on 25.08.17.
 */
public class SortByBitsTest {
    private final SortByBits bits = new SortByBits();

    @Test
    public void givenANumberThatContainsZeroBitsWhenCountOnesInvokedThenReturnZero() {
        assertEquals(0, bits.countOnes(0));
    }

    @Test
    public void givenANumberThatContainsOneBitWhenCountOnesInvokedThenReturnOne() {
        assertEquals(1, bits.countOnes(1));
    }

    @Test
    public void givenANumberThatContainsTwoBitsWhenCountOnesInvokedThenReturnTwo() {
        assertEquals(2, bits.countOnes(5));
    }

    @Test
    public void givenANumberThatContainsThreeBitsWhenCountOnesInvokedThenReturnThree() {
        assertEquals(3, bits.countOnes(7));
    }

    @Test
    public void givenANegativeNumberWhenCountBitsInvokedThenReturnBitsCount() {
        assertEquals(31, bits.countOnes(-3));
    }

    @Test
    public void givenAnArrayOfNumbersWhenSortInvokedThenReturnSortedByBitsArray() {
        final Integer[] actualNumbers = new Integer[]{1, 2, 3, 5, 7, 8, 9, 4, 56, 34, 23};
        bits.sort(actualNumbers);
        final Integer[] expectedNumbers = new Integer[]{1, 2, 8, 4, 3, 5, 9, 34, 7, 56, 23};
        assertArrayEquals(expectedNumbers, actualNumbers);
    }

    @Test
    public void givenANegativeArrayOfNumbersWhenSortInvokedThenReturnSortedByBitArray() {
        final Integer[] negativeNumbers = new Integer[]{-3, -7, -11, -3, -2, -23, -41};
        bits.sort(negativeNumbers);
        final Integer[] expectedNumbers = new Integer[]{-23, -7, -11, -41, -3, -3, -2};
        assertArrayEquals(expectedNumbers, negativeNumbers);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void givenAnEmptyArrayWhenSortInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        bits.sort(new Integer[]{});
    }

    @Test
    public void givenNullNumberWhenSortInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        bits.sort(new Integer[]{null, null});
    }

}