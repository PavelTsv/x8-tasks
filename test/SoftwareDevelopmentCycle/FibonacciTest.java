package SoftwareDevelopmentCycle;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by pavel on 23.08.17.
 */
public class FibonacciTest {
    private final Fibonacci fibonacci = new Fibonacci();

    @Test
    public void givenPositiveIndexWhenGetFibonacciInvokedThenReturnFibonacciNumber() {
        final int expectedFibonacciNumber = 5;
        final int index = 5;
        assertEquals(expectedFibonacciNumber, fibonacci.getFibonacci(index));
        assertEquals(0, 0);
    }

    @Test
    public void givenAPositiveFibonacciNumberWhenIsFibNumberInvokedThenReturnTrue() {
        assertTrue(fibonacci.isFibNumber(5));
        assertTrue(fibonacci.isFibNumber(144));
    }

    @Test
    public void givenAPositiveNonFibonacciNumberWhenIsFibNumberInvokedThenReturnFalse() {
        assertFalse(fibonacci.isFibNumber(7));
        assertFalse(fibonacci.isFibNumber(66));
    }

    @Test
    public void givenZeroWhenIsFibNumberInvokedThenReturnTrue() {
        assertTrue(fibonacci.isFibNumber(0));
    }

    private Integer[] matrixElementNeighbours;
    private List<Integer> matrixNeighbours;

    private void checkArrayForFibonacciSequence(Integer[] matrix) {
        matrixElementNeighbours = matrix;
        matrixNeighbours = Arrays.asList(matrixElementNeighbours);
        assertTrue(fibonacci.isFibonacciSequence(matrixNeighbours));
    }

    @Test
    public void givenAFibonacciSequenceArrayWhenIsFibonacciSequenceInvokedThenReturnTrue() {
        checkArrayForFibonacciSequence(new Integer[]{13, 8, 34, 21, 5});
    }

    @Test
    public void givenANonFibonacciSequenceArrayWhenIsFibonacciSequenceInvokedThenReturnFalse() {
        checkArrayForFibonacciSequence(new Integer[]{21, 8, 13, 34});
    }

    private List<Integer> expected;

    private void givenMatrixElementIndexesGetNeighbours(int[][] matrix, int row, int column, List<Integer> expected) {
        assertEquals(expected, fibonacci.getNeighbours(matrix, row, column));
    }

    @Test
    public void givenCornerMatrixIndexWhenGetNeighboursInvokedThenReturnNoOutOfBoundNumbers() {
        expected = Arrays.asList(new Integer[]{1, 2, 3});
        givenMatrixElementIndexesGetNeighbours(new int[][]{{1, 1, 7}, {2, 3, 1}, {11, 4, 3}}, 0, 0, expected);
    }

    @Test
    public void givenCenterOfMatrixIndexWhenGetNeighboursInvokedThenReturnNeighbours() {
        expected = Arrays.asList(new Integer[]{13, 21, 34, 55, 59, 144, 233, 377});
        givenMatrixElementIndexesGetNeighbours(new int[][]{{13, 21, 34}, {55, 5, 59}, {144, 233, 377}}, 1, 1, expected);
    }

    @Test
    public void givenAnEmptyMatrixWhenGetNeighboursInvokedThenReturnEmptyList() {
        expected = Arrays.asList(new Integer[]{});
        givenMatrixElementIndexesGetNeighbours(new int[][]{}, 0, 0, expected);
    }

    @Test
    public void givenAMatrixWhenFindTheFibonacciElementInvokedThenReturnTheElement() {
        int[][] firstMatrix = {{13, 21, 34}, {55, 5, 89}, {144, 233, 377}};
        int firstExpectedResult = 5;
        assertEquals(firstExpectedResult, fibonacci.findTheFibonacciElement(firstMatrix));
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void givenEmptyMatrixWhenFindTheFibonacciElementInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        fibonacci.findTheFibonacciElement(new int[][]{});
    }

    @Test
    public void givenAMatrixWithASingleElementWhenFindFibonacciElementInvokedThenThrowIllegalArgumentException() {
        exception.expect(IllegalArgumentException.class);
        fibonacci.findTheFibonacciElement(new int[][]{{1}});
    }

    @Test
    public void givenEmptyArrayListWhenIsFibonacciSequenceInvokedThenThrowIllegalArgumentException() {
        List<Integer> result = new ArrayList<>();
        exception.expect(IllegalArgumentException.class);
        fibonacci.isFibonacciSequence(result);
    }

    @Test
    public void givenNegativeFibonacciNumbersWhenIsFibonacciSequenceInvokedThenReturnFalse() {
        matrixElementNeighbours = new Integer[]{3, -5, 8, 13};
        matrixNeighbours = Arrays.asList(matrixElementNeighbours);
        assertFalse(fibonacci.isFibonacciSequence(matrixNeighbours));
    }

    @Test
    public void givenTwoNonSequentialFibonacciNumbersWhenIsFibonacciSequenceInvokedThenReturnFalse() {
        matrixElementNeighbours = new Integer[]{0, 3};
        matrixNeighbours = Arrays.asList(matrixElementNeighbours);
        assertFalse(fibonacci.isFibonacciSequence(matrixNeighbours));
    }

    @Test
    public void givenFibonacciNumbersThatDoNotFormASequenceWhenIsFibonacciSequenceInvokedThenReturnFalse() {
        matrixElementNeighbours = new Integer[]{1, 3, 4, 7, 11};
        matrixNeighbours = Arrays.asList(matrixElementNeighbours);
        assertFalse(fibonacci.isFibonacciSequence(matrixNeighbours));
    }

    @Test
    public void givenNegativeFibonacciNumberWhenGetFibonacciInvokedThenThrowIllegalArgumentException() {
        int number = -7;
        exception.expect(IllegalArgumentException.class);
        fibonacci.getFibonacci(number);
    }

    @Test
    public void givenNegativeIndexWhenGetFibonacciInvokedThenThrowIllegalArgumentException() {
        int index = -7;
        exception.expect(IllegalArgumentException.class);
        fibonacci.getFibonacci(index);
    }
}